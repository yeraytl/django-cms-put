from django.apps import AppConfig


class CmsPutConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = 'cms_put'
