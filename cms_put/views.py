from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido

# Create your views here.

formulario = """
<form action="" method="PUT">
    El recurso solicitado no tiene contenido.
    <p>
    Añadir contenido: <input type="text" name="valor">
    <input type="submit" value="Enviar"> 
</form>
"""


@csrf_exempt
def get_content(request, llave):
    
    #PUT
    if request.method == "PUT":
        # almacenamos en la base de datos los datos del cuerpo
        valor = request.POST["valor"]
        # Modelo nuevo
        c = Contenido(clave=llave, valor=valor)
        # Guardamos en la base de datos de la siguiente forma
        c.save()

    #GET
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El recurso " + contenido.clave + " tiene " + contenido.valor + " y la ID es " + str(contenido.id)
    except Contenido.DoesNotExist:
        respuesta = formulario

    return HttpResponse(respuesta)

def index(request):
    return HttpResponse("Esta esa la pagina principal de cms_put.")
